<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

 define('WP_HOME','https://pegasus-technology.com');
 define('WP_SITEURL','https://pegasus-technology.com');

 // Update 8-April-2018: I moved https redirection from the Apache virtual server config to wp-config.php using this snippet.
 if (strpos($_SERVER['HTTP_X_FORWARDED_PROTO'], 'https') !== false)
        $_SERVER['HTTPS']='on';

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'database_name_here' );

/** MySQL database username */
define( 'DB_USER', 'username_here' );

/** MySQL database password */
define( 'DB_PASSWORD', 'password_here' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'aGGb:(y%~crVTN[VMRa9aWMYeO@i9+hP0cWLHtxs~li1WEZ$M5U|]c-7<8sP$!/s' );
define( 'SECURE_AUTH_KEY',  '5BdPwEtGqT,*U&zV*]|^^``)6ufohCLzjO_-Dkb&_mr@(;WW[l|d9.G+RA&7)-+^' );
define( 'LOGGED_IN_KEY',    '-f:c7I39bCd;/K2-%+sG-9Uu=3?*xC|EbO&D9HU?E38:A9S~tBgL#+*aM8%4+BHN' );
define( 'NONCE_KEY',        'n8#c,QGm!Wk]9+M|Y`jn!!q;b;1O[%6M8-:rKE&HCY)PyWgC$vhEgris]r`8#AI.' );
define( 'AUTH_SALT',        '+|(+[@@m%2. P&1SmxBe wI.,!I3DRE{#lk1d)+.%=}/it]U+tf_R+jV,q9lAFP9' );
define( 'SECURE_AUTH_SALT', '{x@Q}id?=hb}r!NyRx{Y71F]gdPgmaW8Ua6 LnRo4MuQKO?^-ZLpU_*|E;63f+|a' );
define( 'LOGGED_IN_SALT',   '3E_hRTZgsqhUjdntS?`4AKo^=2W6qAk>@m/2.w%U%O 3tW/ifl[B@?NMyf.P,DyR' );
define( 'NONCE_SALT',       '7J!+|Fw|VFaXNOa{FKK-nm4 Rn%#i$uQ#@Od_FSZ/j#f~;ccjiQXn[Xf|}rjh$QL' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
        define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
