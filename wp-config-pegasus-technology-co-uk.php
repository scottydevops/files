<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

 define('WP_HOME','https://pegasus-technology.co.uk');
 define('WP_SITEURL','https://pegasus-technology.co.uk');

 // Update 8-April-2018: I moved https redirection from the Apache virtual server config to wp-config.php using this snippet.
 if (strpos($_SERVER['HTTP_X_FORWARDED_PROTO'], 'https') !== false)
        $_SERVER['HTTPS']='on';

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'database_name_here' );

/** MySQL database username */
define( 'DB_USER', 'username_here' );

/** MySQL database password */
define( 'DB_PASSWORD', 'password_here' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'HX*lgQd*SryvDB|4fwEH@_(2GhY/@g`Vt>Eio%3R;_F<MoE71T-=d8)dmf,E:>1w' );
define( 'SECURE_AUTH_KEY',  '&#&E&igIb+}hpiPkD_s9YE;ZC:lTqi|+%0-L$H7M%sIckEjsF91+B~2.)*=>sRA>' );
define( 'LOGGED_IN_KEY',    'mX-x$E!|La;4N*[faw8dHDE6k3UGAx&#z[||inilKE>+)()O6x}b+3=5EAAYC4n6' );
define( 'NONCE_KEY',        'z 3*Wvhvl Ja&$2pR%^R~b-;1EQiP5ba(-edYz#UVjP:,%PAku0,KcGsvp0StVQ%' );
define( 'AUTH_SALT',        '(5%.jmi=en%?Am WZ|a!&>f;X#$t|9/-*+<!^F<Z{XD|nRY/|[m6o,JGi>_gJ.^;' );
define( 'SECURE_AUTH_SALT', ']Yzd|r BDsk`re8M>S%WQdL4*[7r76TVP&Aj{gt7E==C4zRNSK5#LSu|;3H2M{Q1' );
define( 'LOGGED_IN_SALT',   ')1/yR},@6mDH}d{aoH40#9WD3a}i<BHTB<&(@;Mk`_Jj.)qvfj5;-^yP9-o3SNkH' );
define( 'NONCE_SALT',       'e3{_xBSICgE2Nm,cw=Gri-Lk-8DliG=xLyGo~ln24W->g&$@QMDzC[q&X~`sTk2=' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
        define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
