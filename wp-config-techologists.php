<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

 define('WP_HOME','https://techologists.co.uk');
 define('WP_SITEURL','https://techologists.co.uk');

 // Update 8-April-2018: I moved https redirection from the Apache virtual server config to wp-config.php using this snippet.
 if (strpos($_SERVER['HTTP_X_FORWARDED_PROTO'], 'https') !== false)
        $_SERVER['HTTPS']='on';

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'database_name_here' );

/** MySQL database username */
define( 'DB_USER', 'username_here' );

/** MySQL database password */
define( 'DB_PASSWORD', 'password_here' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '}+V{cabfT.)KHZ{5wBW!*Z}^}VW>uU_O~u}nf}TK 2JG$yG;)b$~U?{`(B?V&s[v' );
define( 'SECURE_AUTH_KEY',  'L|l.bNNmDg.s<Kep%eRvm++)`gr<`M1aW$:29ZfuE~G_?Xn-~2H1d|Y7,di8)-C_' );
define( 'LOGGED_IN_KEY',    '!@8/+I^tJ{%J@Gc$(&hkN,wJuZ%FH4cpVvVo!o0OoS-Ny]E|C~UUUr v&GgM?1g?' );
define( 'NONCE_KEY',        's!J8?-Mu)k_q,!M$5S-Cd3P+VS6_2f)}$t4#!+w+dG-qpFV~*v%H`7a{l}rP/]jA' );
define( 'AUTH_SALT',        '*t;kJ/UU+,0S<$?:cweSIcnB|Kc{f`d]q%U+@+Bh5s[yw{M(Y d8ZuK~*YRGO%Sv' );
define( 'SECURE_AUTH_SALT', '+ht->1R=WCH[J{RiumSP)RQ&!q[xc)e%yWlk|Q4?%$C Hb,77X$2l(+eY(v*RMEA' );
define( 'LOGGED_IN_SALT',   '6.DxHjD[*nz4!|`KR3}qh>u*uRK]zXo?m^^u0w0CyR|#{-8R|_o|PE5U<%q!RCQ|' );
define( 'NONCE_SALT',       '[E|F2UPUH1n`>fR_ov6]|?0TRb,y-VFC@z_$-kO:<d7b3V0ghHz30K.qoS>V%Ri6' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
        define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
