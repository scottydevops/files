<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

 define('WP_HOME','https://homefarmproduce.co.uk');
 define('WP_SITEURL','https://homefarmproduce.co.uk');

 // Update 8-April-2018: I moved https redirection from the Apache virtual server config to wp-config.php using this snippet.
 if (strpos($_SERVER['HTTP_X_FORWARDED_PROTO'], 'https') !== false)
        $_SERVER['HTTPS']='on';

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'database_name_here' );

/** MySQL database username */
define( 'DB_USER', 'username_here' );

/** MySQL database password */
define( 'DB_PASSWORD', 'password_here' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '%Rz&3fO:8Wj%a&%pQpTexj]ren+-X(U!+qHP(;8||(^g1-@GS=`aw<n;-nLrmkX@' );
define( 'SECURE_AUTH_KEY',  'ugvZ68qhhmG?]N0Rgl++$ND]SDaOW)Pv78|]fN`qf;0>I@/:_)9|?H)n$bWkBo/|' );
define( 'LOGGED_IN_KEY',    '<M#quCDB%<mhqBO{IbZ`kDuT%aw]^_Ml-LJH.#+q}sgjoX_-aa_6o,%#sL8-NxXN' );
define( 'NONCE_KEY',        'noo+A#,e%MTw5@{[F*rWq= vd]E_y0-13CTDWs[-Vn`.`9]YF_=UxesU)$~Q<&8?' );
define( 'AUTH_SALT',        'jH@ae.X.>,a|X?6t,9JBVM4~_!8&AFC9A--j5~>9`cMN@om=[zT#T#t(S2wYrl:p' );
define( 'SECURE_AUTH_SALT', '@*O f5$tWUml-5@m]!YtC2QtUi!x&6E;x8,_O2YVX>H|PZH6gOA./$#| O2&s}2a' );
define( 'LOGGED_IN_SALT',   '9hFd!*+C]o#q_x$c}^v,ohj!A7kZmUMEVBM>G&?TBV~)sTn}Oj-NBncg{4X=1v3z' );
define( 'NONCE_SALT',       '+h+)_;.|xx#.T-av?/;HG3Bd~]-@OK~+x:h@M</y`!8h0mUj6zs&PTJWP-^_),9d' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
        define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
